README.md
========

https://hub.docker.com/u/travy504

#### pulumi
resources to get started: 
- https://pulumi.io/quickstart/gcp/setup.html
- https://cloud.google.com/compute/docs/regions-zones/

example project used to bootstrap this project:
- https://github.com/pulumi/examples/tree/master/gcp-ts-k8s-ruby-on-rails-postgresql

CI/CD docs to deploy app:
- https://pulumi.io/reference/cd-gitlab-ci.html

integrate with helm:
- https://blog.pulumi.com/using-helm-and-pulumi-to-define-cloud-native-infrastructure-as-code

cloud sql admin api:
- https://console.developers.google.com/apis/api/sqladmin.googleapis.com/overview?project=engaged-arcanum-197821