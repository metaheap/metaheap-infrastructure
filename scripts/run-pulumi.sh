#!/bin/bash

# exit if a command returns a non-zero exit code and also print the commands and their args as they are executed
set -e -x

# Add the pulumi CLI to the PATH
export PATH=$PATH:$HOME/.pulumi/bin

yarn install
pulumi stack select metaheap-dev
# The following is just a sample config setting that the hypothetical pulumi
# program needs.
# Learn more about pulumi configuration at: https://pulumi.io/reference/config.html
pulumi config set gcp:project $GOOGLE_PROJECT
pulumi config set gcp:zone $GOOGLE_REGION-$GOOGLE_ZONE
pulumi config set metaheap:clusterPassword $CLUSTER_PASSWORD --secret
pulumi config set metaheap:dbUsername $DB_USERNAME
pulumi config set metaheap:dbPassword $DB_PASSWORD --secret
pulumi config set metaheap:dockerUsername $DOCKER_USERNAME
pulumi config set metaheap:dockerPassword $DOCKER_PASSWORD --secret
pulumi up